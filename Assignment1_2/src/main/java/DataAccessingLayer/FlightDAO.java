package DataAccessingLayer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import Model.Flight;



public class FlightDAO {
	private static final Log LOGGER = LogFactory.getLog(FlightDAO.class);
	private SessionFactory factory;
	
	
	public FlightDAO(){
		try{
			factory=new Configuration().configure().buildSessionFactory();
		}
		catch(Throwable ex){
			System.err.println("Failed to create sessionFactory object."+ex);
			throw new ExceptionInInitializerError(ex);
		}
		
	}
	
	public void addFlight(Flight flight) {
		Session session = factory.openSession();
		int flightId = -1;
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			flightId=(Integer)session.save(flight);
			flight.setId(flightId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("Error at INSERT Query", e);
		} finally {
			session.close();
		}
	}
	
	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes" })
	public Flight readFlight(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE id = :id");
			query.setParameter("id", id);
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("Error at SELECT Query", e);
		} finally {
			session.close();
		}
		return flights != null && !flights.isEmpty() ? flights.get(0) : null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Flight> readFlights() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> flights =null;
		try {
			tx = session.beginTransaction();
			
		    flights = (List<Flight>)session.createQuery("FROM Flight").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("Error at SELECT Query", e);
		} finally {
			session.close();
		}
		return flights;
	}
	
	
	@SuppressWarnings({ "deprecation", "rawtypes" })
	public void updateFlight(int oldFlightId ,Flight newFlight) {
		
		Session session = factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			
			Query query = session.createQuery("UPDATE Flight SET id = :id, flight_number = :flight_number, airplane_type = :airplane_type, departure_city = :departure_city, departure_date_hour = :departure_date_hour, arrival_city = :arrival_city, arrival_date_hour = :arrival_date_hour where id = :oldId");
			query.setParameter("id",newFlight.getId());
			query.setParameter("flight_number", newFlight.getFlight_number());
			query.setParameter("airplane_type",newFlight.getAirplane_type());
			query.setParameter("departure_city",newFlight.getDeparture_city());
			query.setParameter("departure_date_hour",newFlight.getDeparture_date_hour());
			query.setParameter("arrival_city", newFlight.getArrival_city());
			query.setParameter("arrival_date_hour",newFlight.getArrival_date_hour());
			
			query.setParameter("oldId",oldFlightId);
			
		    query.executeUpdate();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("Error at UPDATE Query", e);
		} finally {
			session.close();
		}
	}
	
	@SuppressWarnings({ "deprecation", "rawtypes" })
	public void deleteFlight(int id)
	{
		Session session=factory.openSession();
		Transaction tx=null;
	    Flight fl=new Flight();
		
	    try{
	    	tx=session.beginTransaction();
	    	Query query=session.createQuery("DELETE from Flight WHERE id = :id");
	    	query.setParameter("id", id);
	    	
	    	int result = query.executeUpdate();
	    	
	    	tx.commit();
	    }
	    catch(HibernateException e) {
	    	if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("Error at DELETE Query", e);
		} finally {
				session.close();
		}
	 
	}
	
	
	
}
