package DataAccessingLayer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import Model.User;


public class UserDAO {
	private static final Log LOGGER = LogFactory.getLog(UserDAO.class);
	private SessionFactory factory;
	
	public UserDAO(){
		try{
			factory=new Configuration().configure().buildSessionFactory();
		}
		catch(Throwable ex){
			System.err.println("Failed to create sessionFactory object"+ex);
			throw new ExceptionInInitializerError();
		}
	}
	
	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes" })
	public User loginUser(String username, String password) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<User> users = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM User WHERE username = :username and password = :password");
			query.setParameter("username", username);
			query.setParameter("password", password);
			
			users = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("Error at SELECT Query", e);
		} finally {
			session.close();
		}
		return users != null && !users.isEmpty() ? users.get(0) : null;
	}

	public SessionFactory getFactory() {
		return factory;
	}

	public void setFactory(SessionFactory factory) {
		this.factory = factory;
	}

}
