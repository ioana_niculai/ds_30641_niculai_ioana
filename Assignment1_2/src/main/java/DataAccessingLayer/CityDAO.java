package DataAccessingLayer;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import Model.City;


public class CityDAO {
	private static final Log LOGGER = LogFactory.getLog(UserDAO.class);
	private SessionFactory factory;
	
	public CityDAO(){
		try{
			factory=new Configuration().configure().buildSessionFactory();
		}
		catch(Throwable ex){
			System.err.println("Failed to create sessionFactory object"+ex);
			throw new ExceptionInInitializerError();
		}
	}
	
	
	public City getCity(String name) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<City> cities = null;
		
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM City WHERE name = :name");
			query.setParameter("name", name);
		
			cities = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("Error at SELECT Query from City Table", e);
		} finally {
			session.close();
		}
		return cities != null && !cities.isEmpty() ? cities.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public List<City> readCities() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<City> cities =null;
		try {
			tx = session.beginTransaction();
			
		    cities = (List<City>)session.createQuery("FROM City").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("Error at SELECT Query", e);
		} finally {
			session.close();
		}
		return cities;
	}
	
	public SessionFactory getFactory() {
		return factory;
	}
	public void setFactory(SessionFactory factory) {
		this.factory = factory;
	}
	
}
