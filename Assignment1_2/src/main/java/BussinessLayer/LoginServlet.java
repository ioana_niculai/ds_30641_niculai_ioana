package BussinessLayer;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DataAccessingLayer.UserDAO;
import Model.User;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDAO userDAO = new UserDAO();

	
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		String user = request.getParameter("user");
		String pass = request.getParameter("pass");
		
		try {
			User resultLogin = new User();
			resultLogin = userDAO.loginUser(user, pass);

			if (resultLogin.getRole().equals("Admin")) {
				
				HttpSession session=request.getSession();  
		        session.setAttribute("userType","ADMIN"); 
		        session.setMaxInactiveInterval(30*60);
				System.out.println("\n\n** Session created for "+ session.getAttribute("userType"));
				
				response.sendRedirect("helloAdmin.jsp");
			} 
			else if (resultLogin.getRole().equals("Client")) {
				
				HttpSession session=request.getSession();  
		        session.setAttribute("userType", "CLIENT");  
		        session.setMaxInactiveInterval(30*60);
				System.out.println("\n\n** Session created for "+ session.getAttribute("userType"));
				
				response.sendRedirect("helloClient.html");
			}
			
		} catch (NullPointerException ex) {
			response.sendRedirect("wrongCredential.html");
		}

	}
	
	protected void validateSession(){
		
		
	}
}
