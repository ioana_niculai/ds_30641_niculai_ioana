package BussinessLayer;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DataAccessingLayer.FlightDAO;


@WebServlet("/DeleteFlightServlet")
public class DeleteFlightServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FlightDAO flightDAO=new FlightDAO();  

	
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		javax.servlet.RequestDispatcher rd = getServletContext().getRequestDispatcher(
				"/deleteFlight.html");
		rd.include(request, response);
	}
	
	
	@Override
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		try{
		String id=request.getParameter("id");

		flightDAO.deleteFlight(Integer.parseInt(id));
		System.out.println("Deleted successfully!");
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				"/helloAdmin.jsp");
		rd.include(request, response);
		
		}
		catch(Exception e)
		{
			
			RequestDispatcher rd = getServletContext().getRequestDispatcher(
					"/deleteFlight.html");
			rd.include(request, response);
		}
	}
	


}
