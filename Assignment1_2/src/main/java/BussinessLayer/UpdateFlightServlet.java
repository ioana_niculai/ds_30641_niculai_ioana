package BussinessLayer;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DataAccessingLayer.FlightDAO;
import Model.Flight;


@WebServlet("/UpdateFlightServlet")
public class UpdateFlightServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FlightDAO flightDAO=new FlightDAO();  

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		javax.servlet.RequestDispatcher rd = getServletContext().getRequestDispatcher(
				"/updateFlight.html");
		rd.include(request, response);
	}
	
	
	@Override
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		try{
			
		String old_id=request.getParameter("old_id");
		
		String id=request.getParameter("id");
		String fNumber=request.getParameter("flight_number");
		String aType=request.getParameter("airplane_type");
		String dCity=request.getParameter("departure_city");
		String dDateHour=request.getParameter("departure_date_hour");
		String aCity=request.getParameter("arrival_city");
		String aDateHour=request.getParameter("arrival_date_hour");
		
		Timestamp ts1 = Timestamp.valueOf(dDateHour);
		Timestamp ts2 = Timestamp.valueOf(aDateHour);
	
		
		Flight flightToAdd=new Flight();
		flightToAdd.setId(Integer.parseInt(id));
		flightToAdd.setFlight_number(fNumber);
		flightToAdd.setAirplane_type(aType);
		flightToAdd.setDeparture_city(dCity);
		flightToAdd.setDeparture_date_hour(ts1);
		flightToAdd.setArrival_city(aCity);
		flightToAdd.setArrival_date_hour(ts2);

		flightDAO.updateFlight(Integer.parseInt(old_id),flightToAdd);
		
		System.out.println("Update successfully !");
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				"/helloAdmin.jsp");
		rd.include(request, response);
		
		}
		catch(Exception e)
		{
			RequestDispatcher rd = getServletContext().getRequestDispatcher(
					"/updateFlight.html");
			rd.include(request, response);
		}
	}
	


}
