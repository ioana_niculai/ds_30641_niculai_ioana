package BussinessLayer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import DataAccessingLayer.CityDAO;
import Model.City;




@WebServlet("/LocalTimeServlet")
public class LocalTimeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CityDAO cityDAO = new CityDAO();

	
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		javax.servlet.RequestDispatcher rd = getServletContext().getRequestDispatcher(
				"/findLocalTime.html");
		rd.include(request, response);
	}
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			
			String chosen_city=request.getParameter("chosen_city");

			City rez=cityDAO.getCity(chosen_city);
			
			double latitude=rez.getLatitude();
			double longitude=rez.getLongitude();
			
			String s= "http://api.timezonedb.com/v2/get-time-zone?key=2PKPXPFY1IW1&format=json&by=position&lat=" + latitude + "&lng=" + longitude;
            URL url_external_service=new URL(s);
			
		
	        HttpURLConnection con = (HttpURLConnection)url_external_service.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
		    String input = br.readLine();
			
		    JSONParser parser = new JSONParser();
		    JSONObject json=null;
		    
			try {
				json = (JSONObject) parser.parse(input);
			} catch (ParseException e) {
				e.printStackTrace();
			}
						
		    String local_time_formatted= json.get("formatted").toString();
		    
			PrintWriter out=response.getWriter();
		    out.print("Local time for city " +rez.getName()+ " is: "+ local_time_formatted);
		    
		    
		} catch (NullPointerException e) {
			javax.servlet.RequestDispatcher rd = getServletContext().getRequestDispatcher(
					"/findLocalTime.html");
			rd.include(request, response);
		}
	}
	

}
