package BussinessLayer;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DataAccessingLayer.CityDAO;
import Model.City;



@WebServlet("/ViewCitiesServlet")
public class ViewCitiesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CityDAO cityDAO = new CityDAO();

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<City> cities=cityDAO.readCities();

		PrintWriter out=response.getWriter();
		out.print("<html><body>"); 
		out.print("<form action='ViewCitiesServlet'method='get'>"); 
		out.print("<table border='1'"); 
		out.print("<tr><TH>Id</TH><TH>Name</TH><TH>Latitude</TH><TH>Longitude</TH></TR>"); 
		for (int i=0;i<cities.size();i++) 
		{ 
		out.print("<tr><td>"+cities.get(i).getId()+"</td><td>"+cities.get(i).getName()+"</td><td>"+cities.get(i).getLatitude()+"</td><td>"+cities.get(i).getLongitude()+"</td></td>"); 
		} 
	
		out.print("</table>"); 
		out.print("</form>");
		out.print("</body></html>"); 
	
	}
}
