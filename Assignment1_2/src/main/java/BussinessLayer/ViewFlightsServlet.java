package BussinessLayer;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DataAccessingLayer.FlightDAO;
import Model.Flight;

@WebServlet("/ViewFlightsServlet")
public class ViewFlightsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FlightDAO flightDAO = new FlightDAO();

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Flight> flights=flightDAO.readFlights();

		PrintWriter out=response.getWriter();
		out.print("<html><body>"); 
		out.print("<form action='ViewFlightsServlet'method='get'>"); 
		out.print("<table border='1'"); 
		out.print("<tr><th>Id</TH><TH>Flight number</TH><TH>Airplane Type</TH><TH>Departure City</TH><TH>Departure Date Hour</TH><TH>Arrival City</TH><TH>Arrival Date Hour</TH></TR>"); 
		for (int i=0;i<flights.size();i++) 
		{ 
		out.print("<tr><td>"+flights.get(i).getId()+"</td><td>"+flights.get(i).getFlight_number()+"</td><td>"+flights.get(i).getAirplane_type()+"</td><td>"+flights.get(i).getDeparture_city()+"</td><td>"+flights.get(i).getDeparture_date_hour()+"</td><td>"+flights.get(i).getArrival_city()+"</td><td>"+flights.get(i).getArrival_date_hour()+"</td>"); 
		} 
	
		out.print("</table>"); 
		out.print("</form>");
		out.print("</body></html>"); 
	
	}
}
