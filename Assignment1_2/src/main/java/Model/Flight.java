package Model;

import java.sql.Timestamp;

public class Flight {

	private int id;
	private String flight_number;
	private String airplane_type;
	private String departure_city;
	private Timestamp departure_date_hour;
	private String arrival_city;
	private Timestamp arrival_date_hour;
	
	public Flight(){}
	
	
	public Flight(int id, String flight_number, String airplane_type,
			String departure_city, Timestamp departure_date_hour,
			String arrival_city, Timestamp arrival_date_hour) {
		this.id = id;
		this.flight_number = flight_number;
		this.airplane_type = airplane_type;
		this.departure_city = departure_city;
		this.departure_date_hour = departure_date_hour;
		this.arrival_city = arrival_city;
		this.arrival_date_hour = arrival_date_hour;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFlight_number() {
		return flight_number;
	}
	public void setFlight_number(String flight_number) {
		this.flight_number = flight_number;
	}
	public String getAirplane_type() {
		return airplane_type;
	}
	public void setAirplane_type(String airplane_type) {
		this.airplane_type = airplane_type;
	}
	public String getDeparture_city() {
		return departure_city;
	}
	public void setDeparture_city(String departure_city) {
		this.departure_city = departure_city;
	}
	public Timestamp getDeparture_date_hour() {
		return departure_date_hour;
	}
	public void setDeparture_date_hour(Timestamp departure_date_hour) {
		this.departure_date_hour = departure_date_hour;
	}
	public String getArrival_city() {
		return arrival_city;
	}
	public void setArrival_city(String arrival_city) {
		this.arrival_city = arrival_city;
	}
	public Timestamp getArrival_date_hour() {
		return arrival_date_hour;
	}
	public void setArrival_date_hour(Timestamp arrival_date_hour) {
		this.arrival_date_hour = arrival_date_hour;
	}
	public String toString(){
		return "\n\nFlight: \n id="+this.getId()+ 
				        "\n flight_number="+ this.getFlight_number()+
				        "\n airplane_type="+ this.getAirplane_type()+
				        "\n departure city="+this.getDeparture_city()+
				        "\n departure_date_hour="+this.getDeparture_date_hour()+ 
				        "\n arrival city="+this.getArrival_city()+
				        "\n arrival_date_hour"+this.getArrival_date_hour();
	}

}
