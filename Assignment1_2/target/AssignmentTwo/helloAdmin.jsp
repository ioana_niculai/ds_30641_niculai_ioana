<%@ page language="java" import="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
<style>
.button1 {
	background-color: white;
	color: black;
	border: 2px solid #4CAF50;
}

.button2 {
	background-color: white;
	color: black;
	border: 2px solid #008CBA;
}

.button3 {
	background-color: white;
	color: black;
	border: 2px solid #f44336;
}

.button4 {
	background-color: white;
	color: black;
	border: 2px solid #555555;
}

table,td,th,tr {
	border: 1px solid black;
	border-collapse: collapse
}

table {
	width: 100%;
}

th {
	height: 50px;
}
</style>
</head>

<body>

<% if ( !request.getSession().getAttribute("userType").equals("ADMIN")) 
 { response.sendRedirect("accessDenied.html");
  System.out.println("** Acces denied for: "+request.getSession().getAttribute("userType"));
 }
  else 
 {
 System.out.println("** Acces OK for "+request.getSession().getAttribute("userType"));
 }

%>

	Hi, Admin

	<form action="ViewFlightsServlet" method="get">
		<button class="button1" type="submit">View flights</button>
	</form>

	<form action="AddFlightServlet" method="post">
		<button class="button2" type="submit">Create flight</button>
	</form>

	<form action="UpdateFlightServlet" method="post">
		<button class="button3" type="submit">Update flight</button>
	</form>

	<form action="DeleteFlightServlet" method="post">
		<button class="button4" type="submit">Delete flight</button>
	</form>

	<form action="LogoutServlet" method="post">
		<input type="submit" value="Logout">
	</form>

</body>
</html>