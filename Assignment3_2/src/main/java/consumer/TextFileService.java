package consumer;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class TextFileService {

	public void createTextFile(String fileName, String contentFile) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(fileName, "UTF-8");
			writer.println(contentFile);
			writer.close();
			
			 System.out.println("Text written.");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	

}
