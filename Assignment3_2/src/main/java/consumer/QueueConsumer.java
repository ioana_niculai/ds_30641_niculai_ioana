package consumer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang.SerializationUtils;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;

import queue.EndPoint;

public class QueueConsumer extends EndPoint implements Runnable, Consumer {
	
	
	
	public QueueConsumer(String endPointName) throws IOException {
		super(endPointName);
	}

	
	public void run() {
		try {
			channel.basicConsume(endPointName, true, this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void handleConsumeOk(String consumerTag) {
		//System.out.println("Consumer " + consumerTag + " registered\n");
	}


	public void handleDelivery(String consumerTag, Envelope env, BasicProperties props, byte[] body)
			throws IOException {
		Map map = (HashMap) SerializationUtils.deserialize(body);
		
		MailService mailService = new MailService("email@gmail.com","password");
		TextFileService fileService=new TextFileService();
		
		System.out.println("\nSending mail**");
		mailService.sendMail("email@gmail.com","Assignment3_2",""+SerializationUtils.deserialize(body));

		
		 String filepath = "D:\\workspace3_2DS\\Assignment3_2\\results\\";
		
		Random rand = new Random();
		int nrRandom = rand.nextInt(100);
	
		System.out.println("\nWriting into file ** "+"file"+nrRandom+".txt **");
		fileService.createTextFile(filepath+"file"+nrRandom+".txt",""+SerializationUtils.deserialize(body));
		
	}

	public void handleCancel(String consumerTag) {
	}

	public void handleCancelOk(String consumerTag) {
	}

	public void handleRecoverOk(String consumerTag) {
	}

	public void handleShutdownSignal(String consumerTag, ShutdownSignalException arg1) {
	}
}