package model;

public class DVD {
	private String Title;
	private int Year;
	private double Price;

	public DVD(String title, int year, double price) {
		Title = title;
		Year = year;
		Price = price;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public int getYear() {
		return Year;
	}

	public void setYear(int year) {
		Year = year;
	}

	public double getPrice() {
		return Price;
	}

	public void setPrice(double price) {
		Price = price;
	}
	
	public String toString(){
		return " Title: " + this.getTitle() +
				"\n Year: "+this.getYear()+
				"\n Price: "+this.getPrice();
	}
}
