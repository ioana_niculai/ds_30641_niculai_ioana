package main;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import model.DVD;
import consumer.QueueConsumer;
import producer.Producer;

@WebServlet("/AddDVDServlet")
public class AppStart extends HttpServlet{

	public AppStart() {

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		javax.servlet.RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.jsp");
		rd.include(request, response);
	}

	
	@Override
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		try{
			String title=request.getParameter("title");
			String syear=request.getParameter("year");
			String sprice=request.getParameter("price");
		
			int year=Integer.parseInt(syear);
			double price=Double.parseDouble(sprice);
			try {
				QueueConsumer consumer = new QueueConsumer("queue");
				Thread consumerThread = new Thread(consumer);
				consumerThread.start();

				Producer producer = new Producer("queue");
				HashMap<String, Integer> message = new HashMap<String, Integer>();

				DVD dvd1 = new DVD(title,Integer.parseInt(syear),Double.parseDouble(sprice));
				message.put(dvd1.toString(), 1);
				producer.sendMessage(message);

				
			} catch (IOException e) {
				e.printStackTrace();
			}
			RequestDispatcher rd = getServletContext().getRequestDispatcher(
					"/index.jsp");
			rd.include(request, response);
			
		}
		catch(Exception e)
		{
			RequestDispatcher rd = getServletContext().getRequestDispatcher(
					"/index.jsp");
			rd.include(request, response);
		}
	}
}