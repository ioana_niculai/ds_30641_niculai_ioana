(function() {
	var userServices = angular.module('userServices', []);

	userServices.factory('UserFactory', [ '$http', 'config',
			function($http, config) {

				var privateUserDetails = function(id) {
					return $http.get(config.API_URL + '/user/details/' + id);
				};

				var privateUserList = function() {
					return $http.get(config.API_URL + '/user/all');
				};

				var privateDeleteUser=function(id){
					return $http.delete(config.API_URL+ '/user/delete/' + id);			
				};
				
				var privateLoginUser=function(username,password){
					return $http.get(config.API_URL + '/user/login/' + username+'/'+password);
				};
				
				
				var privateAddUser=function(user){
					return $http.post(config.API_URL + '/user/insert',user);
				};
				
				var privateUpdateUser=function(id,newUser){
					return $http.put(config.API_URL + '/user/update/'+id,newUser);
				};
				
				return {
					findById : function(id) {
						return privateUserDetails(id);
					},

					findAll : function() {
						return privateUserList();
					},
					deleteById: function(id){
						return privateDeleteUser(id);						
					},
					
					loginUser: function(username,password)
					{
						return privateLoginUser(username, password)
					},
				
					addNewUser: function(user){
						return privateAddUser(user);
					},
					updateUser: function(id, newUser)
					{
						return privateUpdateUser(id, newUser);
					}
				};
			} ]);

})();