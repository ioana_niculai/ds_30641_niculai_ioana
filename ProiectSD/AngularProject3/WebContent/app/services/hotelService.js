(function() {
	var hotelServices = angular.module('hotelServices', []);

	hotelServices.factory('HotelFactory', [ '$http', 'config',
			function($http, config) {

				var privateHotelDetails = function(id) {
					return $http.get(config.API_URL + '/hotel/details/' + id);
				};

				var privateHotelList = function() {
					return $http.get(config.API_URL + '/hotel/all');
				};

				var privateDeleteHotel=function(id){
					return $http.delete(config.API_URL+ '/hotel/delete/' + id);			
				};

				var privateAddHotel=function(hotel){
					return $http.post(config.API_URL + '/hotel/insert',hotel);
				};
				
				var privateUpdateHotel=function(id,newHotel){
					return $http.put(config.API_URL + '/hotel/update/'+id,newHotel);
				};
				
				
				
				return {
					findById : function(id) {
						return privateHotelDetails(id);
					},

					findAll : function() {
						return privateHotelList();
					},
					deleteById: function(id){
						return privateDeleteHotel(id);						
					},

					addNewHotel: function(hotel){
						return privateAddHotel(hotel);
					},
					updateHotel: function(id, newHotel)
					{
						return privateUpdateHotel(id, newHotel);
					}
				};
			} ]);

})();