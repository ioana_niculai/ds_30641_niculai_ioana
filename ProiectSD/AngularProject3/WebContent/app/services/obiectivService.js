(function() {
	var objectiveServices = angular.module('obiectivServices', []);

	objectiveServices.factory('ObjectiveFactory', [ '$http', 'config',
			function($http, config) {

				var privateObjectiveDetails = function(id) {
					return $http.get(config.API_URL + '/obiectiv/details/' + id);
				};

				var privateObjectiveList = function() {
					return $http.get(config.API_URL + '/obiectiv/all');
				};

				var privateDeleteObjective=function(id){
					return $http.delete(config.API_URL+ '/obiectiv/delete/' + id);			
				};
				var privateAddObjective=function(obiectiv){
					return $http.post(config.API_URL + '/obiectiv/insert',obiectiv);
				};
				var privateUpdateObjective=function(old_id_obj,newObj){
					return $http.put(config.API_URL + '/obiectiv/update/'+old_id_obj,newObj);
				};
				
			
				return {
					findById : function(id) {
						return privateObjectiveDetails(id);
					},

					findAll : function() {
						return privateObjectiveList();
					},
					deleteById: function(id){
						return privateDeleteObjective(id);						
					},
					addNewObjective: function(obiectiv){
						return privateAddObjective(obiectiv);
					},
					updateObjective: function(old_id_obj, newObj)
					{
						return privateUpdateObjective(old_id_obj, newObj);
					}
					
				};
			} ]);

})();