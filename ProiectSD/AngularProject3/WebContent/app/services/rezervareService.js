(function() {
	var rezervareServices = angular.module('rezervareServices', []);

	rezervareServices.factory('rezervareFactory', [ '$http', 'config',
			function($http, config) {

				var privateRezervareDetails = function(id) {
					return $http.get(config.API_URL + '/rezervare/details/' + id);
				};

				var privateRezervareList = function() {
					return $http.get(config.API_URL + '/rezervare/all');
				};

				var privateDeleteRezervare=function(id){
					return $http.delete(config.API_URL+ '/rezervare/delete/' + id);			
				};
				
				var privateAddReservation=function(reservation){
					return $http.post(config.API_URL + '/rezervare/insert',reservation);
				};	
				
				var privateUpdateReservation=function(rezervationOldid,newReservation){
					return $http.put(config.API_URL + '/rezervare/update/'+rezervationOldid,newReservation);
				};
				
				var privatePrintReservations=function(){
					return $http.post(config.API_URL + '/rezervare/print');
				};
				
				return {
					findById : function(id) {
						return privateRezervareDetails(id);
					},

					findAll : function() {
						return privateRezervareList();
					},
					deleteById: function(id){
						return privateDeleteRezervare(id);						
					},
					addNewReservation: function(reservation){
						return privateAddReservation(reservation);
					},
					updateReservation: function(rezervationOldid, newReservation)
					{
						return privateUpdateReservation(rezervationOldid, newReservation);
					},
					printInvoice: function(){
						return privatePrintReservations();
					}
				};
			} ]);

})();