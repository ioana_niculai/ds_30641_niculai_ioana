(function() {
	var orasServices = angular.module('orasServices', []);

	orasServices.factory('CityFactory', [ '$http', 'config',
			function($http, config) {

				var privateOrasDetails = function(id) {
					return $http.get(config.API_URL + '/oras/details/' + id);
				};

				var privateOrasList = function() {
					return $http.get(config.API_URL + '/oras/all');
				};
					
				return {
					findById : function(id) {
						return privateOrasDetails(id);
					},

					findAll : function() {
						return privateOrasList();
					}
				};
			} ]);

})();