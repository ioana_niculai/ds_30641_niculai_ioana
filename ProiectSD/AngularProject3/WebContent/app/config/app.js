(function() {
	var app = angular.module('angularjs-project', [ 'ngRoute', 'configModule',
			'navControllers', 'userControllers' , 'userServices','hotelControllers' , 'hotelServices','rezervareControllers' , 'rezervareServices','obiectivControllers' , 'obiectivServices','orasControllers' , 'orasServices'])
})();