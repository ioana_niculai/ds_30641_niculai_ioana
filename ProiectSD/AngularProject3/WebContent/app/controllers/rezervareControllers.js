(function() {

	var reservationsModule = angular.module('rezervareControllers',
			[ 'ngRoute' ])

	reservationsModule
			.config(function($routeProvider) {
				$routeProvider
						.when(
								'/reservations',
								{
									templateUrl : 'app/views/reservation/reservation-list.html',
									controller : 'AllRezervariController',
									controllerAs : "allRezervariCtrl"
								})
						.when(
								'/reservation/:id',
								{
									templateUrl : 'app/views/reservation/reservation-details.html',
									controller : 'RezervareController',
									controllerAs : "rezervareCtrl"
								})

						.when(
								'/reservationsAdmin',
								{
									templateUrl : 'app/views/reservation/reservation-list-Admin.html',
									controller : 'AllRezervariController',
									controllerAs : "allRezervariCtrl"
								})
						.when(
								'/reservationAdmin/:id',
								{
									templateUrl : 'app/views/reservation/reservation-details.html',
									controller : 'RezervareController',
									controllerAs : "rezervareCtrl"
								})

						.when(
								'/reservation/deleteReservation/:id',
								{
									templateUrl : 'app/views/reservation/reservation-list.html',
									controller : 'DeleteRezervareController',
									controllerAs : "deleteRezervareCtrl"

								})
						.when(
								'/reservation/adaugareRezervare/:nume_persoana/:nume_hotel/:nr_locuri_rezervate/:pret_calculat/:id_user',
								{
									templateUrl : 'app/views/reservation/reservation-list.html',
									controller : 'AddRezervareController',
									controllerAs : "addRezervareCtrl"
								})
						.when(
								'/reservation/updateReservation/:reservation_old_id/:new_nume_persoana/:new_nume_hotel/:new_nr_locuri_rezervate/:new_pret_calculat/:new_id_user',
								{
									templateUrl : 'app/views/reservation/reservation-list.html',
									controller : 'UpdateRezervareController',
									controllerAs : "updateRezervareCtrl"
								})
						.when(
								'/printInvoice',
								{
									templateUrl : 'app/views/reservation/reservation-list.html',
									controller : 'PrintInvoiceController',
									controllerAs : "printInvoiceRezervareCtrl"
								})

			});

	reservationsModule.controller('AllRezervariController', [ '$scope',
			'rezervareFactory', function($scope, rezervareFactory) {
				$scope.reservations = [];
				var promise = rezervareFactory.findAll();
				promise.success(function(data) {
					$scope.reservations = data;
				}).error(function(data, status, header, config) {
					alert(status);
				});

			} ]);

	reservationsModule.controller('RezervareController', [ '$scope',
			'$routeParams', 'rezervareFactory',
			function($scope, $routeParams, rezervareFactory) {
				var id = $routeParams.id;
				var promise = rezervareFactory.findById(id);
				$scope.rezervare = null;
				promise.success(function(data) {
					$scope.rezervare = data;
				}).error(function(data, status, header, config) {
					alert(status);
				});
			} ]);

	reservationsModule.controller('DeleteRezervareController', [ '$scope',
			'$routeParams', 'rezervareFactory',
			function($scope, $routeParams, rezervareFactory) {
				var id = $routeParams.id;
				var promise = rezervareFactory.deleteById(id);
				$scope.rezervare = null;
				promise.success(function() {
					alert("Stergere cu succes!");
				}).error(function(data, status, header, config) {
					alert(status);
				});
			} ]);

	reservationsModule.controller('AddRezervareController', [ '$routeParams',
			'rezervareFactory', function($routeParams, rezervareFactory) {

				var rezervare = {
					nume_persoana : $routeParams.nume_persoana,
					nume_hotel : $routeParams.nume_hotel,
					nr_locuri_rezervate : $routeParams.nr_locuri_rezervate,
					pret_calculat : $routeParams.pret_calculat,
					id_user : $routeParams.id_user
				};

				var promise = rezervareFactory.addNewReservation(rezervare);

				promise.success(function(data, status, header, config) {
					alert("Adaugare cu succes!");
				}).error(function(data, status, header, config) {
					alert("Eroare la adaugare:\n " + status);
				});
			} ]);

	reservationsModule.controller('UpdateRezervareController', [
			'$routeParams',
			'rezervareFactory',
			function($routeParams, rezervareFactory) {

				var reservation_old_id = $routeParams.reservation_old_id;

				var newRezervare = {
					nume_persoana : $routeParams.new_nume_persoana,
					nume_hotel : $routeParams.new_nume_hotel,
					nr_locuri_rezervate : $routeParams.new_nr_locuri_rezervate,
					pret_calculat : $routeParams.new_pret_calculat,
					id_user : $routeParams.new_id_user
				};

				var promise = rezervareFactory.updateReservation(
						reservation_old_id, newRezervare);

				alert("rezervation old id:" + reservation_old_id);
				promise.success(function(data, status, header, config) {
					alert("Actualizare cu succes!");
				}).error(function(data, status, header, config) {
					alert("Eroare la actualizare:\n " + status);
				});
			} ]);

	reservationsModule.controller('PrintInvoiceController', [
			'$routeParams',
			'rezervareFactory',
			function($routeParams, rezervareFactory) {

				var promise = rezervareFactory.printInvoice();

				promise.success(function(data, status, header, config) {
					alert("Printare facturi cu succes. Verificati folderul <<facturi>> !");
				}).error(function(data, status, header, config) {
					alert("Eroare la printarea facturilor:\n " + status);
				});
			} ]);

})();
