(function() {

	var citiesModule = angular.module('orasControllers', [ 'ngRoute' ])

	citiesModule.config(function($routeProvider) {
		$routeProvider.when('/cities', {
			templateUrl : 'app/views/city/city-list.html',
			controller : 'AllCitiesController',
			controllerAs : "allCitiesCtrl"
		}).when('/city/:id', {
			templateUrl : 'app/views/city/city-details.html',
			controller : 'CityController',
			controllerAs : "cityCtrl"
		})
	});

	citiesModule.controller('AllCitiesController', [ '$scope', 'CityFactory',
			function($scope, CityFactory) {
				$scope.cities = [];
				var promise = CityFactory.findAll();
				promise.success(function(data) {
					$scope.cities = data;
				}).error(function(data, status, headesr, config) {
					alert(status);
				});

			} ]);

	citiesModule.controller('CityController', [ '$scope', '$routeParams',
			'CityFactory', function($scope, $routeParams, CityFactory) {
				var id = $routeParams.id;
				var promise = CityFactory.findById(id);
				$scope.city = null;
				promise.success(function(data) {
					$scope.city = data;
				}).error(function(data, status, header, config) {
					alert(status);
				});
			} ]);


})();
