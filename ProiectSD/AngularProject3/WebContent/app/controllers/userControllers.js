(function() {

	var usersModule = angular.module('userControllers', [ 'ngRoute' ])

	
	usersModule
			.config(function($routeProvider) {
				$routeProvider
						.when('/users', {
							templateUrl : 'app/views/user/user-list.html',
							controller : 'AllUsersController',
							controllerAs : "allUsersCtrl"
						})
						.when('/user/:id', {
							templateUrl : 'app/views/user/user-details.html',
							controller : 'UserController',
							controllerAs : "userCtrl"
						})
						.when('/user/deleteUser/:id', {
							templateUrl : 'app/views/user/user-list.html',
							controller : 'DeleteUserController',
							controllerAs : "deleteUserCtrl"
						})
						.when('/user/login/:username/:password', {
							template: '',
							controller : 'LoginController',
							controllerAs : "loginCtrl"
						})
						.when(
								'/user/adaugareUser/:username/:password/:fullname/:role/:email',
								{
									templateUrl : 'app/views/user/user-list.html',
									controller : 'AddUserController',
									controllerAs : "addUserCtrl"
								})
						.when(
								'/user/updateUser/:old_id/:new_username/:new_password/:new_fullname/:new_role/:new_email',
								{
									templateUrl : 'app/views/user/user-list.html',
									controller : 'UpdateUserController',
									controllerAs : "updateUserCtrl"
								})
						.when('/backToLogin',
								{
							template: '',
							controller : 'BackToLoginController',
							controllerAs : "backToLoginCtrl"
						})
					
			});

	usersModule.controller('AllUsersController', [ '$scope', 'UserFactory',
			function($scope, UserFactory) {
				$scope.users = [];
				var promise = UserFactory.findAll();
				promise.success(function(data) {
					$scope.users = data;
				}).error(function(data, status, header, config) {
					alert(status);
				});

			} ]);

	usersModule.controller('UserController', [ '$scope', '$routeParams',
			'UserFactory', function($scope, $routeParams, UserFactory) {
				var id = $routeParams.id;
				var promise = UserFactory.findById(id);
				$scope.user = null;
				promise.success(function(data) {
					$scope.user = data;
				}).error(function(data, status, header, config) {
					alert(status);
				});
			} ]);

	usersModule.controller('DeleteUserController', [ '$scope', '$routeParams',
			'UserFactory', function($scope, $routeParams, UserFactory) {
				var id = $routeParams.id;
				var promise = UserFactory.deleteById(id);
				$scope.user = null;
				promise.success(function() {
					alert("Stergere cu succes!");
				}).error(function(data, status, header, config) {
					alert(status);
				});
			} ]);

	usersModule.controller('LoginController', ['$scope', '$window','$location', '$routeParams',
			'UserFactory', function($scope, $window,$location,$routeParams, UserFactory) {
				var username = $routeParams.username;
				var password = $routeParams.password;
				
				var promise = UserFactory.loginUser(username, password);

				promise.success(function(data, status, header, config) {
					if ("admin" == data.role) {
						alert("Logare cu succes! Rolul tau este ADMIN.");
						// $location.path('/indexAdmin.html');
						 $window.location.href = "http://localhost:8080/AngularProject3/indexAdmin.html#/";
					} else if ("traveler" == data.role) {
						alert("Logare cu succes! Rolul tau este TRAVELER.");
						 $window.location.href = "http://localhost:8080/AngularProject3/indexTraveler.html#/";
					}
				}).error(function(data, status, header, config) {
					alert("Username sau parola gresita.Incearca din nou!\n ");
					$window.location.href = "http://localhost:8080/AngularProject3/loginErrorPage.html#/";
				});
			} ]);

	
	usersModule.controller('BackToLoginController', ['$scope', '$window','$location', '$routeParams',
		'UserFactory', function($scope, $window,$location,$routeParams, UserFactory) {
	
		    $window.location.href = "http://localhost:8080/AngularProject3/#/";
		} ]);
	

	
	usersModule.controller('AddUserController', [ '$routeParams',
			'UserFactory', function($routeParams, UserFactory) {

				var user = {
					username : $routeParams.username,
					password : $routeParams.password,
					fullname : $routeParams.fullname,
					role : $routeParams.role,
					email : $routeParams.email
				};

				var promise = UserFactory.addNewUser(user);

				promise.success(function(data, status, header, config) {
					alert("Adaugare cu succes!");
				}).error(function(data, status, header, config) {
					alert("Eroare la adaugare:\n " + status);
				});
			} ]);

	usersModule.controller('UpdateUserController', [ '$routeParams',
			'UserFactory', function($routeParams, UserFactory) {

				var old_id = $routeParams.old_id;

				var newUser = {
					username : $routeParams.new_username,
					password : $routeParams.new_password,
					fullname : $routeParams.new_fullname,
					role : $routeParams.new_role,
					email : $routeParams.new_email
				};

				var promise = UserFactory.updateUser(old_id, newUser);

				promise.success(function(data, status, header, config) {
					alert("Actualizare cu succes!");
				}).error(function(data, status, header, config) {
					alert("Eroare la actualizare:\n " + status);
				});
			} ]);

})();
