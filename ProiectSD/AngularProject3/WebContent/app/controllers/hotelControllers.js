(function() {

	var hotelsModule = angular.module('hotelControllers', [ 'ngRoute' ])

	hotelsModule.config(function($routeProvider) {
		$routeProvider.when('/hotels', {
			templateUrl : 'app/views/hotel/hotel-list.html',
			controller : 'AllHotelsController',
			controllerAs : "allHotelsCtrl"
		}).when('/hotelsTraveler', {
			templateUrl : 'app/views/hotel/hotel-list-Traveler.html',
			controller : 'AllHotelsController',
			controllerAs : "allHotelsCtrl"
		})
		.when('/hotel/:id', {
			templateUrl : 'app/views/hotel/hotel-details.html',
			controller : 'HotelController',
			controllerAs : "hotelCtrl"
		}).when('/hotel/deleteHotel/:id',{
			templateUrl : 'app/views/hotel/hotel-list.html',
			controller: 'DeleteHotelController',
			controllerAs: "deleteHotelCtrl"
		}).when('/hotel/adaugareHotel/:nume/:adresa/:oras/:nr_locuri_disponibile/:pret_camera_2_pers/:pret_camera_3_pers/:id_oras',{
			templateUrl : 'app/views/hotel/hotel-list.html',
			controller : 'AddHotelController',
			controllerAs : "addHotelCtrl"
		}).when('/hotel/updateHotel/:old_id_hotel/:new_nume/:new_adresa/:new_oras/:new_nr_locuri_disponibile/:new_pret_camera_2_pers/:new_pret_camera_3_pers/:new_id_oras',{
			templateUrl : 'app/views/hotel/hotel-list.html',
			controller : 'UpdateHotelController',
			controllerAs : "updateHotelCtrl"
		})
	});

	hotelsModule.controller('AllHotelsController', [ '$scope', 'HotelFactory',
			function($scope, HotelFactory) {
				$scope.hotels = [];
				var promise = HotelFactory.findAll();
				promise.success(function(data) {
					$scope.hotels = data;
				}).error(function(data, status, header, config) {
					alert(status);
				});

			} ]);

	hotelsModule.controller('HotelController', [ '$scope', '$routeParams',
			'HotelFactory', function($scope, $routeParams, HotelFactory) {
				var id = $routeParams.id;
				var promise = HotelFactory.findById(id);
				$scope.hotel = null;
				promise.success(function(data) {
					$scope.hotel = data;
				}).error(function(data, status, header, config) {
					alert(status);
				});
			} ]);
	
	
	
	hotelsModule.controller('DeleteHotelController', [ '$scope', '$routeParams',
		'HotelFactory', function($scope, $routeParams, HotelFactory) {
			var id=$routeParams.id;
			var promise=HotelFactory.deleteById(id);
			$scope.hotel=null;
			promise.success(function() {
				alert("Stergere cu succes!");
			}).error(function(data, status, header, config) {
				alert(status);
			});
	} ]);
	
	hotelsModule.controller('AddHotelController', ['$routeParams',
		'HotelFactory', function($routeParams, HotelFactory) {

			var hotel  = {
					nume: $routeParams.nume,
					adresa: $routeParams.adresa,
					oras: $routeParams.oras,
					nr_locuri_disponibile: $routeParams.nr_locuri_disponibile, 
					pret_camera_2_pers: $routeParams.pret_camera_2_pers,
					pret_camera_3_pers: $routeParams.pret_camera_3_pers,
					id_oras: $routeParams.id_oras,
					
				};
			
			var promise=HotelFactory.addNewHotel(hotel);
			
			promise.success(function(data, status, header, config) {
				alert("Adaugare cu succes!");
			}).error(function(data, status, header, config) {
				alert("Eroare la adaugare:\n "+ status);
			});
	} ]);
	
	hotelsModule.controller('UpdateHotelController', ['$routeParams',
		'HotelFactory', function($routeParams, HotelFactory) {

			var old_id_hotel=$routeParams.old_id_hotel;
		
			var newHotel  = {
					nume: $routeParams.new_nume,
					adresa: $routeParams.new_adresa,
					oras: $routeParams.new_oras,
					nr_locuri_disponibile: $routeParams.new_nr_locuri_disponibile, 
					pret_camera_2_pers: $routeParams.new_pret_camera_2_pers,
					pret_camera_3_pers: $routeParams.new_pret_camera_3_pers,
					id_oras: $routeParams.new_id_oras
				};
			
			var promise=HotelFactory.updateHotel(old_id_hotel,newHotel);
			
			promise.success(function(data, status, header, config) {
				alert("Actualizare cu succes!");
			}).error(function(data, status, header, config) {
				alert("Eroare la actualizare:\n "+ status);
			});
	} ]);
	
	
})();
