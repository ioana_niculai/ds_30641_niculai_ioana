(function() {

	var objectivesModule = angular.module('obiectivControllers', [ 'ngRoute' ])

	objectivesModule.config(function($routeProvider) {
		$routeProvider.when('/objectives', {
			templateUrl : 'app/views/objective/objective-list.html',
			controller : 'AllObjectivesController',
			controllerAs : "allObjectivesCtrl"
		}).when('/objectivesTraveler', {
			templateUrl : 'app/views/objective/objective-list-Traveler.html',
			controller : 'AllObjectivesController',
			controllerAs : "allObjectivesCtrl"
		}).when('/objective/:id', {
			templateUrl : 'app/views/objective/objective-details.html',
			controller : 'ObjectiveController',
			controllerAs : "objectiveCtrl"
		}).when('/objective/deleteObjective/:id', {
			templateUrl : 'app/views/objective/objective-list.html',
			controller : 'DeleteObjectiveController',
			controllerAs : "deleteObjectiveCtrl"

		}).when('/objective/adaugareObiectiv/:nume/:adresa/:oras/:id_oras',{
			templateUrl : 'app/views/objective/objective-list.html',
			controller : 'AddObiectivController',
			controllerAs : "addObiectivCtrl"
		}).when('/objective/updateObjective/:old_id_objective/:new_nume/:new_adresa/:new_oras/:new_id_oras',{
			templateUrl : 'app/views/objective/objective-list.html',
			controller : 'UpdateObiectivController',
			controllerAs : "updateObiectivCtrl"
		})
	});

	objectivesModule.controller('AllObjectivesController', [ '$scope', 'ObjectiveFactory',
			function($scope, ObjectiveFactory) {
				$scope.objectives = [];
				var promise = ObjectiveFactory.findAll();
				promise.success(function(data) {
					$scope.objectives = data;
				}).error(function(data, status, headesr, config) {
					alert(status);
				});

			} ]);

	objectivesModule.controller('ObjectiveController', [ '$scope', '$routeParams',
			'ObjectiveFactory', function($scope, $routeParams, ObjectiveFactory) {
				var id = $routeParams.id;
				var promise = ObjectiveFactory.findById(id);
				$scope.objective = null;
				promise.success(function(data) {
					$scope.objective = data;
				}).error(function(data, status, header, config) {
					alert(status);
				});
			} ]);

	objectivesModule.controller('DeleteObjectiveController', [ '$scope',
			'$routeParams', 'ObjectiveFactory',
			function($scope, $routeParams, ObjectiveFactory) {
				var id = $routeParams.id;
				var promise = ObjectiveFactory.deleteById(id);
				$scope.objective = null;
				promise.success(function() {
					alert("Stergere cu succes!");
				}).error(function(data, status, header, config) {
					alert(status);
				});
			} ]);
	
	
	
	objectivesModule.controller('AddObiectivController', [ '$scope', '$routeParams',
		'ObjectiveFactory', function($scope, $routeParams,ObjectiveFactory) {

			var obiectiv = {
				  nume: $routeParams.nume,
				  adresa: $routeParams.adresa,
				  oras: $routeParams.oras,
				  id_oras: $routeParams.id_oras
			};
			
			var promise=ObjectiveFactory.addNewObjective(obiectiv);
			
			promise.success(function() {
				alert("Adaugare cu succes!");
			}).error(function(data, status, header, config) {
				alert("Eroare la adaugare"+ status);
			});
	} ]);

	
	objectivesModule.controller('UpdateObiectivController', ['$routeParams',
		'ObjectiveFactory', function($routeParams, ObjectiveFactory) {

			var old_id_objective=$routeParams.old_id_objective;
		
			var newObjective = {
					nume: $routeParams.new_nume,
					adresa: $routeParams.new_adresa,
					oras: $routeParams.new_oras,
					id_oras: $routeParams.new_id_oras
				};
			
			var promise=ObjectiveFactory.updateObjective(old_id_objective,newObjective);
			
			promise.success(function(data, status, header, config) {
				alert("Actualizare cu succes!");
			}).error(function(data, status, header, config) {
				alert("Eroare la actualizare:\n "+ status);
			});
	} ]);
	
})();
