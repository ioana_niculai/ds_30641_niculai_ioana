package spring.demo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="obiectivturistic")
public class ObiectivTuristic {
	public int id;
	public String nume;
	public String adresa;
	public String oras;
	public int id_oras;
	
	
	public ObiectivTuristic(){
		
	}
	
	public ObiectivTuristic(int id, String nume, String adresa, String oras, int id_oras) {
		this.id = id;
		this.nume = nume;
		this.adresa = adresa;
		this.oras = oras;
		this.id_oras = id_oras;
	}


	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	
	
	@Column(name = "nume", nullable = false, length = 100)
	public String getNume() {
		return nume;
	}


	public void setNume(String nume) {
		this.nume = nume;
	}

	
	@Column(name = "adresa", nullable = false, length = 100)
	public String getAdresa() {
		return adresa;
	}


	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}


	@Column(name = "oras", nullable = false, length = 100)
	public String getOras() {
		return oras;
	}


	public void setOras(String oras) {
		this.oras = oras;
	}

	
	@Column(name = "id_oras", nullable = false)
	public int getId_oras() {
		return id_oras;
	}


	public void setId_oras(int id_oras) {
		this.id_oras = id_oras;
	}

	
	public String toString(){
		return "\nObiectiv turistic"+
			   "\n id: "+this.getId()+
			   "\n nume: "+this.getNume()+
			   "\n adresa: "+this.getAdresa()+
			   "\n oras: "+this.getOras()+
			   "\n id_oras: "+this.getId_oras();
				
	}
	
}
