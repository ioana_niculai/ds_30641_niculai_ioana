package spring.demo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="oras")
public class Oras {
	private int id;
	private String nume;
	private String judet;
	private String tara;
	private int populatie;
	
	
	public Oras(){
		
	}
	public Oras(int id, String nume, String judet, String tara, int populatie) {
		this.id = id;
		this.nume = nume;
		this.judet = judet;
		this.tara = tara;
		this.populatie = populatie;
	}
	
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	@Column(name = "nume", nullable = false, length = 100)
	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	
	@Column(name = "judet", nullable = false, length = 100)
	public String getJudet() {
		return judet;
	}

	public void setJudet(String judet) {
		this.judet = judet;
	}

	
	@Column(name = "tara", nullable = false, length = 100)
	public String getTara() {
		return tara;
	}

	public void setTara(String tara) {
		this.tara = tara;
	}
	@Column(name = "populatie", nullable = false)
	public int getPopulatie() {
		return populatie;
	}

	public void setPopulatie(int populatie) {
		this.populatie = populatie;
	}
	
	public String toString(){
		return "\nOras"+
			   "\n id: "+this.getId()+
			   "\n nume: "+this.getNume()+
			   "\n judet: "+this.getJudet()+
			   "\n tara: "+this.getTara()+
			   "\n populatie: "+this.getPopulatie();			
	}
	

}
