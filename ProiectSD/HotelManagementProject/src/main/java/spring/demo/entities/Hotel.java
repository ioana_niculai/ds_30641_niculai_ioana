package spring.demo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hotel")
public class Hotel  implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	public int id;
	public String nume;
	public String adresa;
	public String oras;
	public int nr_locuri_disponibile;
	public int pret_camera_2_pers;
	public int pret_camera_3_pers;
	public int id_oras;

	public Hotel() {
	}

	public Hotel(int id, String nume, String adresa, String oras, int nr_locuri_disponibile, int pret_camera_2_pers,
			int pret_camera_3_pers, int id_oras) {
		this.id = id;
		this.nume = nume;
		this.adresa = adresa;
		this.oras = oras;
		this.nr_locuri_disponibile = nr_locuri_disponibile;
		this.pret_camera_2_pers = pret_camera_2_pers;
		this.pret_camera_3_pers = pret_camera_3_pers;
		this.id_oras = id_oras;
	}
	
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	@Column(name = "nume", nullable = false, length = 100)
	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	
	@Column(name = "adresa", nullable = false, length = 100)
	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	
	@Column(name = "oras", nullable = false, length = 100)
	public String getOras() {
		return oras;
	}

	public void setOras(String oras) {
		this.oras = oras;
	}

	
	@Column(name = "nr_locuri_disponibile", nullable = false)
	public int getNr_locuri_disponibile() {
		return nr_locuri_disponibile;
	}

	public void setNr_locuri_disponibile(int nr_locuri_disponibile) {
		this.nr_locuri_disponibile = nr_locuri_disponibile;
	}

	
	@Column(name = "pret_camera_2_pers", nullable = false)
	public int getPret_camera_2_pers() {
		return pret_camera_2_pers;
	}

	public void setPret_camera_2_pers(int pret_camera_2_pers) {
		this.pret_camera_2_pers = pret_camera_2_pers;
	}

	
	@Column(name = "pret_camera_3_pers", nullable = false)
	public int getPret_camera_3_pers() {
		return pret_camera_3_pers;
	}

	public void setPret_camera_3_pers(int pret_camera_3_pers) {
		this.pret_camera_3_pers = pret_camera_3_pers;
	}

	
	@Column(name = "id_oras", nullable = false)
	public int getId_oras() {
		return id_oras;
	}

	public void setId_oras(int id_oras) {
		this.id_oras = id_oras;
	}
	
	
	public String toString(){
		return "\nHotel"+
			   "\n id: "+this.getId()+
			   "\n nume: "+this.getNume()+
			   "\n adresa: "+this.getAdresa()+
			   "\n oras: "+this.getOras()+
			   "\n nr_locuri_disponibile: "+this.getNr_locuri_disponibile()+
			   "\n pret_camera_2_pers: "+this.getPret_camera_2_pers()+
			   "\n pret_camera_3_pers: "+this.getPret_camera_3_pers()+
			   "\n id_oras: "+this.getId_oras();
				
	}

}
