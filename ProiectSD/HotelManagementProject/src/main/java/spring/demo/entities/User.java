package spring.demo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "user")
public class User  implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String username;
	private String password;
	private String fullname;
	private String role;
	private String email;
	
	public User(){}
	
	public User(Integer id, String username, String password,String fullname, String role,String email) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.fullname=fullname;
		this.role = role;
		this.email=email;
	}
	
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	@Column(name = "username", nullable = false, length = 100)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column(name = "password", nullable = false, length = 100)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "role", nullable = false, length = 100)
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
	@Column(name = "fullname", nullable = false, length = 100)
	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	
	@Column(name = "email", nullable = false, length = 100)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String toString(){
	   return "\nUser" + 
			   "\n id:"+this.getId()+
			  "\n fullname:"+this.getFullname()+
   			  "\n username:"+this.getUsername()+
			  "\n password:"+this.getPassword()+
			  "\n role:"+this.getRole()+
			  "\n email:"+this.getEmail();
	}

}
