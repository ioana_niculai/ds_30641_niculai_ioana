package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.demo.entities.Hotel;


public interface HotelRepository extends JpaRepository<Hotel, Integer> {
	Hotel findById(int id);
}
