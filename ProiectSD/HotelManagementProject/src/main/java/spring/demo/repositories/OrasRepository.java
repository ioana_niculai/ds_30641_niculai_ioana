package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.demo.entities.Oras;

public interface OrasRepository extends JpaRepository<Oras, Integer> {
	Oras findById(int id);
}
