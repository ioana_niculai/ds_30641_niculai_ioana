package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.demo.entities.ObiectivTuristic;


public interface ObiectivTuristicRepository extends JpaRepository<ObiectivTuristic, Integer> {
	ObiectivTuristic findById(int id);
}
