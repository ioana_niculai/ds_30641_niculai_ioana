package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.demo.entities.Rezervare;


public interface RezervareRepository extends JpaRepository<Rezervare, Integer> {
	Rezervare findById(int id);
}
