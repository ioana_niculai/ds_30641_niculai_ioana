package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.demo.entities.User;


public interface UserRepository extends JpaRepository<User, Integer> {

	User findById(int id);

	User findByUsernameAndPassword(String username, String password);
	
}
