package spring.demo.dto;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class RezervareDTO {

	private int id;
	private String nume_persoana;
	private String nume_hotel;
	private int nr_locuri_rezervate;
	private int pret_calculat;
	private int id_user;

	public RezervareDTO() {

	}

	public RezervareDTO(int id, String nume_persoana, String nume_hotel, int nr_locuri_rezervate, int pret_calculat,
			int id_user) {
		this.id = id;
		this.nume_persoana = nume_persoana;
		this.nume_hotel = nume_hotel;
		this.nr_locuri_rezervate = nr_locuri_rezervate;
		this.pret_calculat = pret_calculat;
		this.id_user = id_user;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "nume_persoana", nullable = false, length = 100)
	public String getNume_persoana() {
		return nume_persoana;
	}

	public void setNume_persoana(String nume_persoana) {
		this.nume_persoana = nume_persoana;
	}

	@Column(name = "nume_hotel", nullable = false, length = 100)
	public String getNume_hotel() {
		return nume_hotel;
	}

	public void setNume_hotel(String nume_hotel) {
		this.nume_hotel = nume_hotel;
	}

	@Column(name = "nr_locuri_rezervate", nullable = false)
	public int getNr_locuri_rezervate() {
		return nr_locuri_rezervate;
	}

	public void setNr_locuri_rezervate(int nr_locuri_rezervate) {
		this.nr_locuri_rezervate = nr_locuri_rezervate;
	}

	@Column(name = "pret_calculat", nullable = false)
	public int getPret_calculat() {
		return pret_calculat;
	}

	public void setPret_calculat(int pret_calculat) {
		this.pret_calculat = pret_calculat;
	}

	@Column(name = "id_user", nullable = false)
	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	public String toString() {
		return "\nRezervare hotel" + "\n id: " + this.getId() + "\n nume_persoana: " + this.getNume_persoana()
				+ "\n nume_hotel: " + this.getNume_hotel() + "\n nr_locuri_rezervate: " + this.getNr_locuri_rezervate()
				+ "\n pret_calculat: " + this.getPret_calculat() + "\n id_user: " + this.getId_user();
	}

	public static class Builder {
		private int nestedid;
		private String nestednume_persoana;
		private String nestednume_hotel;
		private int nestednr_locuri_rezervate;
		private int nestedpret_calculat;
		private int nestedid_user;

		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}

		public Builder nume_persoana(String nume_persoana) {
			this.nestednume_persoana = nume_persoana;
			return this;
		}

		public Builder nume_hotel(String nume_hotel) {
			this.nestednume_hotel = nume_hotel;
			return this;
		}

		public Builder nr_locuri_rezervate(int nr_locuri_rezervate) {
			this.nestednr_locuri_rezervate = nr_locuri_rezervate;
			return this;
		}

		public Builder pret_calculat(int pret_calculat) {
			this.nestedpret_calculat = pret_calculat;
			return this;
		}

		public Builder id_user(int id_user) {
			this.nestedid_user = id_user;
			return this;
		}

		public RezervareDTO create() {
			return new RezervareDTO(nestedid, nestednume_persoana, nestednume_hotel, nestednr_locuri_rezervate,
					nestedpret_calculat, nestedid_user);
		}

	}
}
