package spring.demo.dto;

public class OrasDTO {

	private int id;
	private String nume;
	private String judet;
	private String tara;
	private int populatie;

	public OrasDTO() {

	}

	public OrasDTO(int id, String nume, String judet, String tara, int populatie) {
		this.id = id;
		this.nume = nume;
		this.judet = judet;
		this.tara = tara;
		this.populatie = populatie;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getJudet() {
		return judet;
	}

	public void setJudet(String judet) {
		this.judet = judet;
	}

	public String getTara() {
		return tara;
	}

	public void setTara(String tara) {
		this.tara = tara;
	}

	public int getPopulatie() {
		return populatie;
	}

	public void setPopulatie(int populatie) {
		this.populatie = populatie;
	}

	public String toString() {
		return "\nOras" + "\n id: " + this.getId() + "\n nume: " + this.getNume() + "\n judet: " + this.getJudet()
				+ "\n tara: " + this.getTara() + "\n populatie: " + this.getPopulatie();
	}
	

	public static class Builder{
		private int nestedid;
		private String nestednume;
		private String nestedjudet;
		private String nestedtara;
		private int nestedpopulatie;

		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}
		
		public Builder nume(String nume) {
			this.nestednume = nume;
			return this;
		}
		
		public Builder judet(String judet) {
			this.nestedjudet = judet;
			return this;
		}
		
		public Builder tara(String tara) {
			this.nestedtara = tara;
			return this;
		}
		
		public Builder populatie(int populatie) {
			this.nestedpopulatie = populatie;
			return this;
		}
		
		public OrasDTO create(){
			return new OrasDTO(nestedid, nestednume, nestedjudet, nestedtara, nestedpopulatie);
		}
		

	}
	
	
	
	
	
	
}
