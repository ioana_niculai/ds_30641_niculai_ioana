package spring.demo.dto;


public class ObiectivTuristicDTO {

	private int id;
	private String nume;
	private String adresa;
	private String oras;
	private int id_oras;

	public ObiectivTuristicDTO() {
	}

	public ObiectivTuristicDTO(int id, String nume, String adresa, String oras, int id_oras) {
		this.id = id;
		this.nume = nume;
		this.adresa = adresa;
		this.oras = oras;
		this.id_oras = id_oras;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getOras() {
		return oras;
	}

	public void setOras(String oras) {
		this.oras = oras;
	}

	public int getId_oras() {
		return id_oras;
	}

	public void setId_oras(int id_oras) {
		this.id_oras = id_oras;
	}

	public String toString() {
		return "\nObiectiv turistic" + "\n id: " + this.getId() + "\n nume: " + this.getNume() + "\n adresa: "
				+ this.getAdresa() + "\n oras: " + this.getOras() + "\n id_oras: " + this.getId_oras();

	}
	
	
	public static class Builder{
		private int nestedid;
		private String nestednume;
		private String nestedadresa;
		private String nestedoras;
		private int nestedid_oras;
		
		
		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}
		
		public Builder nume(String nume) {
			this.nestednume = nume;
			return this;
		}
		public Builder adresa(String adresa) {
			this.nestedadresa = adresa;
			return this;
		}
		public Builder oras(String oras) {
			this.nestedoras = oras;
			return this;
		}

		public Builder id_oras(int id_oras) {
			this.nestedid_oras = id_oras;
			return this;
		}

		public ObiectivTuristicDTO create(){
			return new ObiectivTuristicDTO(nestedid, nestednume, nestedadresa, nestedoras, nestedid_oras);
		}		
	}

}
