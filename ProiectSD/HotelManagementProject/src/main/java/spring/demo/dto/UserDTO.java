package spring.demo.dto;

public class UserDTO {
	private Integer id;
	private String username;
	private String password;
	private String fullname;
	private String role;
	private String email;

	public UserDTO() {
	}

	public UserDTO(Integer id, String username,String password, String fullname,  String role, String email) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.fullname = fullname;
		this.role = role;
		this.email = email;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String toString() {
		return "\nUser" + "\n id:" + this.getId() + "\n fullname:" + this.getFullname() + "\n username:"
				+ this.getUsername() + "\n password:" + this.getPassword() + "\n role:" + this.getRole() + "\n email:"
				+ this.getEmail();
	}

	public static class Builder {

		private Integer nestedid;
		private String nestedusername;
		private String nestedpassword;
		private String nestedfullname;
		private String nestedrole;
		private String nestedemail;

		public Builder id(Integer id) {
			this.nestedid = id;
			return this;
		}

		public Builder username(String username) {
			this.nestedusername = username;
			return this;
		}

		public Builder password(String password) {
			this.nestedpassword = password;
			return this;
		}

		public Builder fullname(String fullname) {
			this.nestedfullname = fullname;
			return this;
		}

		public Builder role(String role) {
			this.nestedrole = role;
			return this;
		}

		public Builder email(String email) {
			this.nestedemail = email;
			return this;
		}

		public UserDTO create() {
			return new UserDTO(nestedid, nestedusername,nestedpassword, nestedfullname, nestedrole, nestedemail);
		}

	}

}
