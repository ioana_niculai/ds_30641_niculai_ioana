package spring.demo.dto;



public class HotelDTO {
	private int id;
	private String nume;
	private String adresa;
	private String oras;
	private int nr_locuri_disponibile;
	private int pret_camera_2_pers;
	private int pret_camera_3_pers;
	private int id_oras;

	public HotelDTO() {
	}

	public HotelDTO(int id, String nume, String adresa, String oras, int nr_locuri_disponibile, int pret_camera_2_pers,
			int pret_camera_3_pers, int id_oras) {
		this.id = id;
		this.nume = nume;
		this.adresa = adresa;
		this.oras = oras;
		this.nr_locuri_disponibile = nr_locuri_disponibile;
		this.pret_camera_2_pers = pret_camera_2_pers;
		this.pret_camera_3_pers = pret_camera_3_pers;
		this.id_oras = id_oras;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getOras() {
		return oras;
	}

	public void setOras(String oras) {
		this.oras = oras;
	}

	public int getNr_locuri_disponibile() {
		return nr_locuri_disponibile;
	}

	public void setNr_locuri_disponibile(int nr_locuri_disponibile) {
		this.nr_locuri_disponibile = nr_locuri_disponibile;
	}

	public int getPret_camera_2_pers() {
		return pret_camera_2_pers;
	}

	public void setPret_camera_2_pers(int pret_camera_2_pers) {
		this.pret_camera_2_pers = pret_camera_2_pers;
	}

	public int getPret_camera_3_pers() {
		return pret_camera_3_pers;
	}

	public void setPret_camera_3_pers(int pret_camera_3_pers) {
		this.pret_camera_3_pers = pret_camera_3_pers;
	}

	public int getId_oras() {
		return id_oras;
	}

	public void setId_oras(int id_oras) {
		this.id_oras = id_oras;
	}
	public String toString(){
		return "\nHotel"+
			   "\n id: "+this.getId()+
			   "\n nume: "+this.getNume()+
			   "\n adresa: "+this.getAdresa()+
			   "\n oras: "+this.getOras()+
			   "\n nr_locuri_disponibile: "+this.getNr_locuri_disponibile()+
			   "\n pret_camera_2_pers: "+this.getPret_camera_2_pers()+
			   "\n pret_camera_3_pers: "+this.getPret_camera_3_pers()+
			   "\n id_oras: "+this.getId_oras();
				
	}
	
	public static class Builder{
		private int nestedid;
		private String nestednume;
		private String nestedadresa;
		private String nestedoras;
		private int nestednr_locuri_disponibile;
		private int nestedpret_camera_2_pers;
		private int nestedpret_camera_3_pers;
		private int nestedid_oras;

		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}
		public Builder nume(String nume) {
			this.nestednume = nume;
			return this;
		}
		public Builder adresa(String adresa) {
			this.nestedadresa = adresa;
			return this;
		}
		public Builder oras(String oras) {
			this.nestedoras = oras;
			return this;
		}
		public Builder nr_locuri_disponibile(int nr_locuri_disponibile) {
			this.nestednr_locuri_disponibile = nr_locuri_disponibile;
			return this;
		}
		
		public Builder pret_camera_2_pers(int pret_camera_2_pers) {
			this.nestedpret_camera_2_pers = pret_camera_2_pers;
			return this;
		}
		
		public Builder pret_camera_3_pers(int pret_camera_3_pers) {
			this.nestedpret_camera_3_pers = pret_camera_3_pers;
			return this;
		}
		public Builder id_oras(int id_oras) {
			this.nestedid_oras = id_oras;
			return this;
		}
		
		public HotelDTO create(){
			return new HotelDTO(nestedid,nestednume,nestedadresa, nestedoras,nestednr_locuri_disponibile, nestedpret_camera_2_pers, nestedpret_camera_3_pers, nestedid_oras);
		}
		
		
	}
}
