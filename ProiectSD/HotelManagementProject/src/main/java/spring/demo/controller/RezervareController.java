package spring.demo.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import spring.demo.dto.RezervareDTO;
import spring.demo.dto.UserDTO;
import spring.demo.services.RezervareService;
import spring.demo.services.UserService;
import spring.demo.services.TextFileService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/rezervare")
public class RezervareController {

	@Autowired
	private RezervareService rezervareService;

	@Autowired
	private UserService us;

	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public RezervareDTO getRezervareById(@PathVariable("id") int id) {
		return rezervareService.findRezervareById(id);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<RezervareDTO> getAllReservations() {
		return rezervareService.findAll();
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertRezervare(@RequestBody RezervareDTO rezervareDTO) {
		return rezervareService.create(rezervareDTO);
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/print", method = RequestMethod.POST)
	public void printareFactura() {

		// printarea facturilor pt toate rezervarile
		String filepath = "D:\\SD_lab\\Proiect3\\HotelManagementProject\\facturi\\";
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd_MM_yyyy__HH_mm_ss");
		LocalDateTime now = LocalDateTime.now();
		String endpointFile = dtf.format(now);

		List<RezervareDTO> rezervari = rezervareService.findAll();

		for (int i = 0; i < rezervari.size(); i++) {
			int userID = rezervari.get(i).getId_user();

			UserDTO usDTO = us.findUserById(userID);

			String finalFilepath = filepath + endpointFile + "_factura" + rezervari.get(i).getNume_persoana() + ".txt";
			String contentFile = rezervari.get(i).toString();
			printareFactura(finalFilepath, contentFile);

		}

	}

	public static void printareFactura(String file, String content) {
		TextFileService service = new TextFileService();
		service.createTextFile(file, content);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public int deleteRezervare(@PathVariable("id") int id) {
		return rezervareService.deleteRezervareById(id);
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public int updateRezervare(@PathVariable("id") int id, @RequestBody RezervareDTO rezervareDTO) {
		return rezervareService.updateRezervareById(id, rezervareDTO);
	}

}
