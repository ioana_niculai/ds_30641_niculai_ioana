package spring.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import spring.demo.dto.OrasDTO;
import spring.demo.services.OrasService;


@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/oras")
public class OrasController {
	
	@Autowired
	private OrasService orasService;

	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public OrasDTO getOrasById(@PathVariable("id") int id) {
		return orasService.findOrasById(id);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<OrasDTO> getAllCities() {
		return orasService.findAll();
	}
	
}
