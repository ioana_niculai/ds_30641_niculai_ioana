package spring.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import spring.demo.dto.HotelDTO;
import spring.demo.services.HotelService;



@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/hotel")
public class HotelController {
	
	@Autowired
	private HotelService hotelService;

	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public HotelDTO getHotelById(@PathVariable("id") int id) {
		return hotelService.findHotelById(id);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<HotelDTO> getAllHotels() {
		return hotelService.findAll();
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertHotel(@RequestBody HotelDTO hotelDTO) {
		return hotelService.create(hotelDTO);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public int deleteHotel(@PathVariable("id") int id) {
		return hotelService.deleteHotelById(id);
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public int updateHotel(@PathVariable("id") int id,@RequestBody HotelDTO hotelDTO) {
		return hotelService.updateHotelById(id,hotelDTO);
	}
	
	
}
