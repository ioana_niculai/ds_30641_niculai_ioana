package spring.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import spring.demo.dto.UserDTO;
import spring.demo.services.UserService;



@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public UserDTO getUserById(@PathVariable("id") int id) {
		return userService.findUserById(id);
	}


	@RequestMapping(value = "/login/{username}/{password}", method = RequestMethod.GET)
	public UserDTO loginUser(@PathVariable("username") String username,@PathVariable("password") String password) {
		return userService.loginUser(username,password);
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<UserDTO> getAllUsers() {
		return userService.findAll();
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertUser(@RequestBody UserDTO userDTO) {
		return userService.create(userDTO);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public int deleteUser(@PathVariable("id") int id) {
		return userService.deleteUserById(id);
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public int updateUser(@PathVariable("id") int id,@RequestBody UserDTO userDTO) {
		return userService.updateUserById(id,userDTO);
	}
	
	
}
