package spring.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import spring.demo.dto.ObiectivTuristicDTO;
import spring.demo.dto.UserDTO;
import spring.demo.services.MailService;
import spring.demo.services.ObiectivTuristicService;
import spring.demo.services.UserService;



@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/obiectiv")
public class ObiectivTuristicController {
	
	@Autowired
	private ObiectivTuristicService obiectivTuristicService;
	
	@Autowired
	private UserService us;

	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public ObiectivTuristicDTO getObiectivTuristicById(@PathVariable("id") int id) {
		return obiectivTuristicService.findObiectivTuristicById(id);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<ObiectivTuristicDTO> getAllObjectives() {
		return obiectivTuristicService.findAll();
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertObiectiv(@RequestBody ObiectivTuristicDTO obiectivTuristicDTO) {
		
		
		//cand se adauga un obiectiv turistic nou se trimite mail la toti travelerii cu detaliile
		List<UserDTO> allUsers;
		allUsers=us.findAll();

		 for (int i = 0; i < allUsers.size(); i++) 
		 {
			 if (allUsers.get(i).getRole().equals("traveler")) 
			 {
			 System.out.println(allUsers.get(i).toString());
			 String subject=allUsers.get(i).getFullname()+" ,poti vizita acest obiectiv!";
			 trimitereMailTraveler(obiectivTuristicDTO,allUsers.get(i).getEmail(),subject);
			
			 }
		 }	
		
		return obiectivTuristicService.create(obiectivTuristicDTO);
	}

	public static void trimitereMailTraveler(ObiectivTuristicDTO obiectiv, String to, String subject) {
		MailService service = new MailService("your_email_here@gmail.com", "your_password_here");
		service.sendMail(to, subject, obiectiv.toString());

	}

	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public int deleteObiectiv(@PathVariable("id") int id) {
		return obiectivTuristicService.deleteObiectivTuristicById(id);
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public int updateObiectiv(@PathVariable("id") int id,@RequestBody ObiectivTuristicDTO obiectivTuristicDTO) {
		return obiectivTuristicService.updateObiectivTuristicById(id,obiectivTuristicDTO);
	}
	
	
}
