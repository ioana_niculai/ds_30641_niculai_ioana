package spring.demo.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.dto.OrasDTO;
import spring.demo.entities.Oras;
import spring.demo.errorhandler.ResourceNotFoundException;
import spring.demo.repositories.OrasRepository;



@Service
public class OrasService {

	@Autowired
	private OrasRepository orsRepository;

	public OrasDTO findOrasById(int orasId) {
		Oras o = orsRepository.findById(orasId);
		if (o == null) {
			throw new ResourceNotFoundException(Oras.class.getSimpleName());
		}
		OrasDTO dto = new OrasDTO.Builder()
				.id(o.getId())
				.nume(o.getNume())
				.judet(o.getJudet())
				.tara(o.getTara())
				.populatie(o.getPopulatie())
				.create();
	
		return dto;
	}

	public List<OrasDTO> findAll() {
		List<Oras> cities = orsRepository.findAll();
		List<OrasDTO> toReturn = new ArrayList<OrasDTO>();
		for (Oras o : cities) {
			OrasDTO dto = new OrasDTO.Builder()
					.id(o.getId())
					.nume(o.getNume())
					.judet(o.getJudet())
					.tara(o.getTara())
					.populatie(o.getPopulatie())
					.create();
			
			toReturn.add(dto);
		}
		return toReturn;
	}


}
