package spring.demo.services;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.dto.UserDTO;
import spring.demo.entities.User;
import spring.demo.errorhandler.EntityValidationException;
import spring.demo.errorhandler.ResourceNotFoundException;
import spring.demo.repositories.UserRepository;


@Service
public class UserService {
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);
	
	@Autowired
	private UserRepository usrRepository;
	
	public UserDTO findUserById(int userId) {
		User usr = usrRepository.findById(userId);
		if (usr == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}
		UserDTO dto = new UserDTO.Builder()
						.id(usr.getId())
						.username(usr.getUsername())
						.password(usr.getPassword())
						.fullname(usr.getFullname())
						.role(usr.getRole())
						.email(usr.getEmail())
						.create();
		return dto;
	}
	
	public UserDTO loginUser(String username,String password) {
		User usr = usrRepository.findByUsernameAndPassword(username, password);
		if (usr == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}
		UserDTO dto = new UserDTO.Builder()
						.id(usr.getId())
						.username(usr.getUsername())
						.password(usr.getPassword())
						.fullname(usr.getFullname())
						.role(usr.getRole())
						.email(usr.getEmail())
						.create();
		return dto;
	}
	
	public List<UserDTO> findAll() {
		List<User> users = usrRepository.findAll();
		List<UserDTO> toReturn = new ArrayList<UserDTO>();
		for (User user : users) {
			UserDTO dto = new UserDTO.Builder()
					.id(user.getId())
					.username(user.getUsername())
					.password(user.getPassword())
					.fullname(user.getFullname())
					.role(user.getRole())
					.email(user.getEmail())
					.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int create(UserDTO userDTO) {
		List<String> validationErrors = validateUser(userDTO);
		if (!validationErrors.isEmpty()) {
			throw new EntityValidationException(User.class.getSimpleName(),validationErrors);
		}

		User user = new User();
		user.setUsername(userDTO.getUsername());
		user.setPassword(userDTO.getPassword());
		user.setFullname(userDTO.getFullname());
		user.setRole(userDTO.getRole());
		user.setEmail(userDTO.getEmail());

		User usr = usrRepository.save(user);
		return usr.getId();
	}

	public int deleteUserById(int userId) {
		User usr = usrRepository.findById(userId);
		if (usr == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}
	
		usrRepository.delete(userId);
		return usr.getId();
	}
	
	
	public int updateUserById(int oldId,UserDTO userDTO)
	{
		User usr = usrRepository.findById(oldId);
		if (usr == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}
		
		usrRepository.delete(oldId);
		
		User user = new User();
		user.setUsername(userDTO.getUsername());
		user.setPassword(userDTO.getPassword());
		user.setFullname(userDTO.getFullname());
		user.setRole(userDTO.getRole());
		user.setEmail(userDTO.getEmail());

		User usrNou = usrRepository.save(user);
		return usrNou.getId();
	}
	
	
	private List<String> validateUser(UserDTO usr) {
		List<String> validationErrors = new ArrayList<String>();

		if (usr.getUsername() == null || "".equals(usr.getUsername())) {
			validationErrors.add("Username field should not be empty");
		}

		if (usr.getPassword() == null || "".equals(usr.getPassword())) {
			validationErrors.add("Password field should not be empty");
		}

		if (usr.getEmail() == null || !validateEmail(usr.getEmail())) {
			validationErrors.add("Email does not have the correct format.");
		}

		return validationErrors;
	}

	
	public static boolean validateEmail(String email) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
		return matcher.find();
	}

}
