package spring.demo.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.dto.HotelDTO;
import spring.demo.entities.Hotel;

import spring.demo.errorhandler.ResourceNotFoundException;
import spring.demo.repositories.HotelRepository;


@Service
public class HotelService {

	@Autowired
	private HotelRepository htlRepository;

	public HotelDTO findHotelById(int hotelId) {
		Hotel ht = htlRepository.findById(hotelId);
		if (ht == null) {
			throw new ResourceNotFoundException(Hotel.class.getSimpleName());
		}
		HotelDTO dto = new HotelDTO.Builder()
				.id(ht.getId())
				.nume(ht.getNume())
				.adresa(ht.getAdresa())
				.oras(ht.getOras())
				.nr_locuri_disponibile(ht.getNr_locuri_disponibile())
				.pret_camera_2_pers(ht.getPret_camera_2_pers())
				.pret_camera_3_pers(ht.getPret_camera_3_pers())
				.id_oras(ht.getId_oras())
				.create();
		return dto;
	}

	public List<HotelDTO> findAll() {
		List<Hotel> hotels = htlRepository.findAll();
		List<HotelDTO> toReturn = new ArrayList<HotelDTO>();
		for (Hotel ht : hotels) {
			HotelDTO dto = new HotelDTO.Builder()
					.id(ht.getId())
					.nume(ht.getNume())
					.adresa(ht.getAdresa())
					.oras(ht.getOras())
					.nr_locuri_disponibile(ht.getNr_locuri_disponibile())
					.pret_camera_2_pers(ht.getPret_camera_2_pers())
					.pret_camera_3_pers(ht.getPret_camera_3_pers())
					.id_oras(ht.getId_oras()).create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int create(HotelDTO hotelDTO) {
		Hotel hotel = new Hotel();
		
		hotel.setNume(hotelDTO.getNume());
		hotel.setAdresa(hotelDTO.getAdresa());
		hotel.setOras(hotelDTO.getOras());
		hotel.setNr_locuri_disponibile(hotelDTO.getNr_locuri_disponibile());
		hotel.setPret_camera_2_pers(hotelDTO.getPret_camera_2_pers());
		hotel.setPret_camera_3_pers(hotelDTO.getPret_camera_3_pers());
		hotel.setId_oras(hotelDTO.getId_oras());

		Hotel htl = htlRepository.save(hotel);
		return htl.getId();
	}

	
	public int deleteHotelById(int Id) {
		Hotel htl = htlRepository.findById(Id);
		if (htl == null) {
			throw new ResourceNotFoundException(Hotel.class.getSimpleName());
		}

		htlRepository.delete(Id);
		return htl.getId();
	}

	public int updateHotelById(int oldId, HotelDTO hotelDTO) {
		Hotel h = htlRepository.findById(oldId);
		if (h== null) {
			throw new ResourceNotFoundException(Hotel.class.getSimpleName());
		}

		htlRepository.delete(oldId);
		
		Hotel hotel = new Hotel();
		
		hotel.setNume(hotelDTO.getNume());
		hotel.setAdresa(hotelDTO.getAdresa());
		hotel.setOras(hotelDTO.getOras());
		hotel.setNr_locuri_disponibile(hotelDTO.getNr_locuri_disponibile());
		hotel.setPret_camera_2_pers(hotelDTO.getPret_camera_2_pers());
		hotel.setPret_camera_3_pers(hotelDTO.getPret_camera_3_pers());
		hotel.setId_oras(hotelDTO.getId_oras());

		Hotel htlNou = htlRepository.save(hotel);
		return htlNou.getId();
	}

}
