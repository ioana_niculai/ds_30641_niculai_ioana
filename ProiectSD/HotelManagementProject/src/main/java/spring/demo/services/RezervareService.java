package spring.demo.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.dto.RezervareDTO;
import spring.demo.entities.Rezervare;
import spring.demo.errorhandler.ResourceNotFoundException;
import spring.demo.repositories.RezervareRepository;


@Service
public class RezervareService {

	@Autowired
	private RezervareRepository rzRepository;

	public RezervareDTO findRezervareById(int rezervareId) {
		Rezervare r = rzRepository.findById(rezervareId);
		if (r == null) {
			throw new ResourceNotFoundException(Rezervare.class.getSimpleName());
		}
		RezervareDTO dto = new RezervareDTO.Builder()
				.id(r.getId())
				.nume_persoana(r.getNume_persoana())
				.nume_hotel(r.getNume_hotel())
				.nr_locuri_rezervate(r.getNr_locuri_rezervate())
				.pret_calculat(r.getPret_calculat())
				.id_user(r.getId_user())
				.create();
		return dto;
	}

	public List<RezervareDTO> findAll() {
		List<Rezervare> rezervari = rzRepository.findAll();
		List<RezervareDTO> toReturn = new ArrayList<RezervareDTO>();
		for (Rezervare r : rezervari) {
					RezervareDTO dto = new RezervareDTO.Builder()
					.id(r.getId())
					.nume_persoana(r.getNume_persoana())
					.nume_hotel(r.getNume_hotel())
					.nr_locuri_rezervate(r.getNr_locuri_rezervate())
					.pret_calculat(r.getPret_calculat())
					.id_user(r.getId_user())
					.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int create(RezervareDTO rezervareDTO) {
		Rezervare rezervare = new Rezervare();
		rezervare.setNume_persoana(rezervareDTO.getNume_persoana());
		rezervare.setNume_hotel(rezervareDTO.getNume_hotel());
		rezervare.setNr_locuri_rezervate(rezervareDTO.getNr_locuri_rezervate());
		rezervare.setPret_calculat(rezervareDTO.getPret_calculat());
		rezervare.setId_user(rezervareDTO.getId_user());
		
		
		Rezervare rez = rzRepository.save(rezervare);
		return rez.getId();
	}

	
	public int deleteRezervareById(int Id) {
		Rezervare rez = rzRepository.findById(Id);
		if (rez == null) {
			throw new ResourceNotFoundException(Rezervare.class.getSimpleName());
		}

		rzRepository.delete(Id);
		return rez.getId();
	}

	public int updateRezervareById(int oldId, RezervareDTO rezervareDTO) {
		Rezervare r = rzRepository.findById(oldId);
		if (r== null) {
			throw new ResourceNotFoundException(Rezervare.class.getSimpleName());
		}

		rzRepository.delete(oldId);
		
		Rezervare rezervare = new Rezervare();
		rezervare.setNume_persoana(rezervareDTO.getNume_persoana());
		rezervare.setNume_hotel(rezervareDTO.getNume_hotel());
		rezervare.setNr_locuri_rezervate(rezervareDTO.getNr_locuri_rezervate());
		rezervare.setPret_calculat(rezervareDTO.getPret_calculat());
		rezervare.setId_user(rezervareDTO.getId_user());
		
		
		Rezervare rez = rzRepository.save(rezervare);
		return rez.getId();
	}

}
