package spring.demo.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import spring.demo.dto.ObiectivTuristicDTO;
import spring.demo.entities.ObiectivTuristic;
import spring.demo.errorhandler.ResourceNotFoundException;
import spring.demo.repositories.ObiectivTuristicRepository;



@Service
public class ObiectivTuristicService {

	@Autowired
	private ObiectivTuristicRepository obtRepository;

	public ObiectivTuristicDTO findObiectivTuristicById(int Id) {
		ObiectivTuristic ot = obtRepository.findById(Id);
		if (ot == null) {
			throw new ResourceNotFoundException(ObiectivTuristicDTO.class.getSimpleName());
		}
		ObiectivTuristicDTO dto = new ObiectivTuristicDTO.Builder()
				.id(ot.getId())
				.nume(ot.getNume())
				.adresa(ot.getAdresa())
				.oras(ot.getOras())
				.id_oras(ot.getId_oras())
				.create();
		
		return dto;
	}

	public List<ObiectivTuristicDTO> findAll() {
		
		List<ObiectivTuristic> objectives = obtRepository.findAll();
		List<ObiectivTuristicDTO> toReturn = new ArrayList<ObiectivTuristicDTO>();
		
		for (ObiectivTuristic ot : objectives) {
		ObiectivTuristicDTO dto = new ObiectivTuristicDTO.Builder()
				.id(ot.getId())
				.nume(ot.getNume())
				.adresa(ot.getAdresa())
				.oras(ot.getOras())
				.id_oras(ot.getId_oras())
				.create();
			toReturn.add(dto);
		}
		return toReturn;
	}
	
	

	public int create(ObiectivTuristicDTO obiectivTuristicDTO) {
		ObiectivTuristic obiectiv = new ObiectivTuristic();
		obiectiv.setNume(obiectivTuristicDTO.getNume());
		obiectiv.setAdresa(obiectivTuristicDTO.getAdresa());
		obiectiv.setOras(obiectivTuristicDTO.getOras());
		obiectiv.setId_oras(obiectivTuristicDTO.getId_oras());
		
		ObiectivTuristic obt = obtRepository.save(obiectiv);
		
		return obt.getId();
	}



	
	public int deleteObiectivTuristicById(int Id) {
		ObiectivTuristic obt = obtRepository.findById(Id);
		if (obt == null) {
			throw new ResourceNotFoundException(ObiectivTuristic.class.getSimpleName());
		}
		obtRepository.delete(Id);
		return obt.getId();
	}

	public int updateObiectivTuristicById(int oldId, ObiectivTuristicDTO obiectivTuristicDTO) {
		ObiectivTuristic h = obtRepository.findById(oldId);
		if (h== null) {
			throw new ResourceNotFoundException(ObiectivTuristic.class.getSimpleName());
		}
		obtRepository.delete(oldId);
		
		ObiectivTuristic obiectiv = new ObiectivTuristic();
		obiectiv.setNume(obiectivTuristicDTO.getNume());
		obiectiv.setAdresa(obiectivTuristicDTO.getAdresa());
		obiectiv.setOras(obiectivTuristicDTO.getOras());
		obiectiv.setId_oras(obiectivTuristicDTO.getId_oras());

		ObiectivTuristic obt = obtRepository.save(obiectiv);

		return obt.getId();
	}

}
