package spring.demo.services;

import java.io.PrintWriter;

public class TextFileService {

	public void createTextFile(String fileName, String contentFile) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(fileName, "UTF-8");
			writer.println(contentFile);
			writer.close();
			
			 System.out.println("Text written into " +fileName);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	

}
