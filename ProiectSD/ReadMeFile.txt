Hotel Management System 


Pentru rularea proiectului am folosit :
--eclipse neon .3
--jdk: jdk1.8.0_121
--tomcat  8.0.47


Prima data se ruleaza proiectul de back-end, cu ajutorul serverului Tomcat. 
Pe urma, tot prin Tomcat se ruleaza pe server si front-end-ul.
Apoi se deschide UI-ul aplicatiei, se introduc datele pentru logare si in functie de utilizatori, o sa apara pagina corespunzatoare. 