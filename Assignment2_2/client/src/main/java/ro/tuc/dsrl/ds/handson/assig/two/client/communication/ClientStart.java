package ro.tuc.dsrl.ds.handson.assig.two.client.communication;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;

import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ITaxSellService;
import ro.tuc.dsrl.ds.handson.assig.two.rpc.Naming;

import java.io.IOException;

public class ClientStart {
	private static final Log LOGGER = LogFactory.getLog(ClientStart.class);

	private ClientStart() {
	}

	public static void main(String[] args) throws IOException {
		ITaxSellService tsService = null;
		try {
			tsService = Naming.lookup(ITaxSellService.class, ServerConnection.getInstance());
		} catch (Exception e) {
			ServerConnection.getInstance().closeAll();
		}
		
		
		new ClientForm(tsService);
		
	}
}
