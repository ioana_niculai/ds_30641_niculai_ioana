package ro.tuc.dsrl.ds.handson.assig.two.client.communication;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ITaxSellService;
import ro.tuc.dsrl.ds.handson.assig.two.rpc.Naming;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.SocketException;
import java.awt.event.ActionListener;

public class ClientForm extends JFrame {
	private static final long serialVersionUID = 1L;

	private JFrame frame;
	private JLabel label;
	private JLabel labelRezultat;
	private JLabel lblyear;
	private JLabel lblengine;
	private JLabel lblpurchasingPrice;
	private JTextField textYear;
	private JTextField texteng;
	private JTextField textprice;
	private JButton tax;
	private JButton price;
	private JTextField result;

	public ClientForm(ITaxSellService tsService) throws IOException,SocketException {

		frame = new JFrame("Car Computing");

		label = new JLabel("Enter car's data ");
		label.setBounds(100, 5, 200, 50);
		label.setForeground(new Color(129, 21, 21));
		label.setFont(new Font("Monotype Corsiva", Font.BOLD, 20));
		label.setVisible(true);
		frame.getContentPane().add(label);

		lblyear = new JLabel("Fabrication year: ");
		lblyear.setBounds(25, 50, 150, 30);
		lblyear.setForeground(new Color(0, 0, 0));
		lblyear.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		lblyear.setVisible(true);
		frame.getContentPane().add(lblyear);

		lblengine = new JLabel("Engine size: ");
		lblengine.setBounds(25, 80, 100, 30);
		lblengine.setForeground(new Color(0, 0, 0));
		lblengine.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		lblengine.setVisible(true);
		frame.getContentPane().add(lblengine);

		lblpurchasingPrice = new JLabel("Purchasing price: ");
		lblpurchasingPrice.setBounds(25, 110, 150, 30);
		lblpurchasingPrice.setForeground(new Color(0, 0, 0));
		lblpurchasingPrice.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		lblpurchasingPrice.setVisible(true);
		frame.getContentPane().add(lblpurchasingPrice);

		textYear = new JTextField(50);
		textYear.setBounds(140, 55, 100, 20);
		textYear.setVisible(true);
		frame.getContentPane().add(textYear);

		texteng = new JTextField(50);
		texteng.setBounds(140, 85, 100, 20);
		texteng.setVisible(true);
		frame.getContentPane().add(texteng);

		textprice = new JTextField(50);
		textprice.setBounds(140, 115, 100, 20);
		textprice.setVisible(true);
		frame.getContentPane().add(textprice);

		tax = new JButton("Tax");
		tax.setBounds(300, 60, 100, 30);
		tax.setForeground(new Color(0, 0, 0));
		tax.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		frame.getContentPane().add(tax);

		price = new JButton("Selling Price");
		price.setBounds(300, 100, 150, 30);
		price.setForeground(new Color(0, 0, 0));
		price.setFont(new Font("Comic Sans MS", Font.PLAIN, 15));
		frame.getContentPane().add(price);

		labelRezultat = new JLabel("Result");
		labelRezultat.setBounds(100, 140, 200, 50);
		labelRezultat.setForeground(new Color(129, 21, 21));
		labelRezultat.setFont(new Font("Monotype Corsiva", Font.BOLD, 20));
		labelRezultat.setVisible(true);
		frame.getContentPane().add(labelRezultat);

		result = new JTextField(50);
		result.setBounds(70, 200, 100, 20);
		result.setVisible(true);
		frame.getContentPane().add(result);

		tax.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int y = Integer.parseInt(textYear.getText());
				int e = Integer.parseInt(texteng.getText());
				int p = Integer.parseInt(textprice.getText());

				Car c = new Car(y, e, p);

				double r = 0;
				try {
					r = tsService.computeTax(c);
				} catch (Exception ex) {
					System.out.println("***exception " + e);
					try {
						ServerConnection.getInstance().closeAll();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}

				r = Math.floor(r * 100) / 100;
				result.setText(r + "");
				clear();
			}
		});

		price.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int y = Integer.parseInt(textYear.getText());
				int e = Integer.parseInt(texteng.getText());
				int p = Integer.parseInt(textprice.getText());

				Car c = new Car(y, e, p);

				double r = 0;
				try {
					r = tsService.computeSellingPrice(c);
				} catch (Exception ex) {
					System.out.println("***exception " + e);
					try {
						ServerConnection.getInstance().closeAll();
					} catch (IOException e1) {

						e1.printStackTrace();
					}
				}

				r = Math.floor(r * 100) / 100;

				result.setText(r + "");
				clear();
			}

		});

		frame.setBounds(100, 100, 500, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);

	}

	public void clear() {
		textYear.setText("");
		textprice.setText("");
		texteng.setText("");
	}

}
