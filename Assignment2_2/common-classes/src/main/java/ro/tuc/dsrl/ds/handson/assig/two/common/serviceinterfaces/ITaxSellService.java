package ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;

public interface ITaxSellService {
	double computeTax(Car c);
	double computeSellingPrice(Car c);

}
